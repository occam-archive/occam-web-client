# Installation

Here, we will discuss the process of starting your own Occam server.
Regardless of whether or not you are setting up a institution-wide Occam instance or a personal server, the process is generally the same.

Occam has two core pieces: the Daemon and the Web Application.
The Daemon is a background process that does all of the heavy work behind the scenes.
It stores files and keeps track of metadata and generates and deploys virtual machines.
The Web Application is what you are using now: a friendly interface that abstracts the common tasks and organizes your work cleanly.

Each needs to be installed separately but they will work together in the end.
By default, most of the work is done.
Just follow these steps and you will have a rudimentary working server in under an hour.

## Cloning the Daemon Repository

You can clone the Occam source code from any Occam instance, or from our centralized mirror on GitLab.
By using the following command, this will install the same version being used by this server:

```shell
git clone %%occam-git%% occam
```

There is an installation script that takes care of most of the work for you, if it recognizes your operating environment.
Attempt to run this script, and if it reports success, then you're done! Skip ahead to installing the Web Application.

```shell
cd occam
sh ./install.sh
```

## Cloning the Web Application Repository

Just like before, you can acquire the Occam web application (which you are using now) from the server you are currently using.

First, if you are currently in the `occam` directory created in the last section, then navigate back out of the occam directory (you *can* install the web client inside the `occam` directory if you wish, however.)

```shell
cd ..
```

By using the following command, this will pull down the same exact code currently being used by this server:

```shell
git clone %%occam-client-git%% occam-web
```

And then, just as before, there is an install script that attempts to walk you through the steps interactively if it recognizes your environment.

```shell
cd occam-web
sh ./install.sh
```

## Running a Basic Server

When these both installed, you must run both the backend and frontend separately.

First, start an Occam daemon. Navigate to the `occam` directory and run:

```shell
./bin/occam daemon run
```

Which will start an Occam daemon process on a default port.

Then, to start a web-portal, navigate to your `occam-web` directory and run:

```shell
./start.sh
```

Which will start a server on a default port (9292), after which you can open a
browser and navigate to <http://localhost:9292> to visit your instance.

Setting up your server on a more permanent basis depends on your local network
and administration. However, you can find systemd and nginx scripts in the
`docs` directory of the occam-web codebase.
