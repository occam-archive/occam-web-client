# Occam Administrative Server Install

**Time**: 30 minutes
**Date**: August 2022
**Operating System**: Ubuntu 22.04
**Target**: Single Server Deployment

I wrote this installing Occam on a fresh OVH server for https://archived.software so I can somewhat vouch for its success.
Our scripts are designed for the types of servers we personally have. For development we have Arch Linux servers and for deployment we have a mix of CentOS, Arch Linux, and Ubuntu. For each of these, the scripts will attempt to automatically install or build dependencies required.

These instructions are for Ubuntu 22.04 as of August of 2022 (But you are better off with 20.04, I think, as of this same date).

## Instructions

### 1. Fresh Install of Ubuntu 22.04; apt update/upgrade; install 'git' and a text editor, etc

Then you might want to create a user just to run the occam services and become that user for the rest of this document. Our scripts *do* assume an `occam` user is the one running the software, as this is what is true on our production servers. By default, and this is reasonable unless you have other storage needs to special environments, but by default the home directory will be where ALL content is stored. More specifically, the `.occam` directory off of that user's `$HOME` folder, which is likely, then, to be `/home/myuser/.occam`, where `myuser` is your user name.

### 2. Clone the source repository for the Occam daemon (1 minute)

```
git clone https://gitlab.com/occam-archive/occam
```

### 3. Run the Occam install script (3-5 minutes)

This script for Ubuntu will auto-install the dependencies. This compiles docs and installs library dependencies, as well. Creates an SQLite database to start.

```
cd occam; ./install.sh
```

If the account you are installing with does not have `sudo` access, on Ubuntu this will fail. Just use an account with `sudo` access to install elsewhere and then just run `./scripts/install/common.sh` directly. Inspecting the `./scripts/install/ubuntu.sh` script will report which packages it means to install.

### 4. Clone the web client repository (<1 minute)

```
cd ..
git clone https://gitlab.com/occam-archive/occam-web-client
```

### 5. Run the installation there as well. (10 minutes)

Again, for Ubuntu, it will automatically install dependencies which you may have to enter a password for and approve. It will install ruby and npm dependencies, compile and minimize web assets, and compile the JavaScript code for the site.

First, though, we need to install the proper version of Ruby. Our little install script does not do this for Ubuntu. Instead we will use [rvm](https://rvm.io/).

Refer to that website, but the steps I needed were:

```
sudo apt install gnupg2
gpg2 --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB
curl -sSL https://get.rvm.io | bash -s stable
source $HOME/.rvm/scripts/rvm
```

Now navigate to the `occam-web-client` directory.

```
cd occam-web-client
```

Look at that... it probably complains about the version discrepency, so follow the instructions:

```
Required ruby-2.6.3 is not installed.
To install do: 'rvm install "ruby-2.6.3"'
```

Therefore (or whatever it might tell you, if the version is updated) (It may require a sudo account, in which case do it first under that account and again under your embargoed user account) you can run:

```
rvm install ruby-2.6.3
```

**NOTE**: Which for Ubuntu 22.04 was a SUPER STRESS FILLED TIME. But I had to follow [these instructions](https://deanpcmad.com/2022/installing-older-ruby-versions-on-ubuntu-22-04/) to get an old Ruby to work with a newer OpenSSL stack. (And then re-run the `rvm install` command above as: `rvm install ruby-2.6.3 -C --with-openssl-dir=$HOME/.openssl/openssl-1.1.1g`)

Once it compiles and installs, we still have to tell `rvm` to use it:

```
rvm use ruby-2.6.3
```

Phew. Now we continue to install Occam properly:

```
cd occam-web-client; ./install.sh
cd ..
```

### 6. Install PostgreSQL instead of SQLite (10 minutes)

For a production environment, it is likely better to use PostgreSQL over SQLite. So let's do that. Install it:

```
sudo apt install postgresql postgresql-contrib
```

Start and enable the database service:

```
sudo systemctl enable postgresql.service
sudo systemctl start postgresql.service
```

Log into the postgres user so we can make a database:

```
sudo -i -u postgres
```

Create our database account:

```
createuser --pwprompt occam
```

Enter (and remember) the password you want to use.

Create our database:

```
createdb occam
```

Log out of the postgres user and back to our normal environment:

```
logout
```

Update our Occam configuration to use postgres:

```
vim ~/.occam/config.yml
```

And change the database section as such:

```
database:
  adapter: 'postgres'
  host: 'localhost'
  port: 5432
  name: 'occam'
  user: 'occam'
  password: 'asdfasdf'
```

Where the password is, of course, the password you used.

Ok. Now a messy part. We need to install a new Occam dependency.

```
cd occam
source python/bin/activate
pip install psycopg2-binary
```

And re-initialize Occam to rebuild the database:

```
./bin/occam system initialize
```

### 7. Create an admin account for your Occam instance

The first account created will be an administrator account. This account can see all and do all.

```
cd occam
./bin/occam accounts new wilkie
```

It will prompt for a password. Give it one and it will create your account and keys. Later, you can log on as this account or create a less privileged account for normal access. You may want to create a "guest" account.

### 7. Create SSL keys via LetsEncrypt. (2-4 minutes)

We will need some SSL keys for accessing the site via HTTPS. We can use LetsEncrypt to handle this free of charge. Phew.

```
sudo apt install certbot
```

We need to, prior to this step, configure, if we haven't already, our domain name to point to our server. Then we can automatically validate the domain for the certificate. Once that is done, we can get a cert (with your email and domains instead of my examples):

**NOTE**: It will be wise to set up a "proxy" domain, as well. This will link to our normal web server, but because of it being a different domain, can be used for non-authenticated (or token authenticated) requests. This allows JavaScript widget access that is trusted and even allows for some local storage, etc, to be used within an `<iframe>` without sandboxing it... it isn't perfect, but we will set that up in one of the last steps.

```
sudo certbot certonly -n --agree-tos -m wilkie@example.com -d archived.software,zzt.archived.software,proxy.archived.software,www.archived.software --standalone
```

**NOTE**: If you followed ahead and have nginx installed, you may need to stop it in order for the bot to run its own web server for a hot second. (`sudo systemctl stop nginx`). Don't forget to perform the `start` command for nginx once more. (If you haven't installed nginx, this is not necessary.)

Which, upon success, gives us this notice:

```
Certificate is saved at: /etc/letsencrypt/live/archived.software/fullchain.pem
Key is saved at:         /etc/letsencrypt/live/archived.software/privkey.pem
```

Which is important for the next step!

### 8. Install a web server. (5 minutes)

This is kinda skipping a step, but I'd rather have the server set up as soon as possible. I would install nginx, for which there is a configuration provided in the `occam-web-client/docs` folder.

sudo apt install nginx

### 9. Edit the nginx configuration.

Edit the file found in `/etc/nginx/nginx.conf` using `occam-web-client/docs/nginx/nginx.conf` as a guide. On an empty Ubuntu install, you would keep the top set of lines and then replace the rest with the equivalent section in the provided docs. So it looks something like this:

```
user www-data;
worker_processes auto;
pid /run/nginx.pid;
include /etc/nginx/modules-enabled/*.conf;

events {
  worker_connections 1024;
}

http {
  include mime.types;

  ...

}
```

We need to update a few lines. We need to tell it our server name, which is our domain name. So the line that reams `server_name 127.0.0.1` should become:

```
  server {
    listen 443 ssl http2 default_server;
    server_name archived.software;
    root /var/www/occam;
  }
```

And then we need to tell it where our certificates are. Which is the thingy the certbot thing gave us! Look for "YOURCERT" in the configuration. It should be the keys for `ssl_certificate` and `ssl_certificate_key`, which coorespond to the two lines certbot reported. Just plop the file paths in.

```
  ...

    ssl_certificate     /etc/letsencrypt/live/archived.software/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/archived.software/privkey.pem;

  ...
```

There we have it. Make sure the lines end with semicolons, and you are good to go.

And then restart the web-server:

```
sudo systemctl restart nginx
```

If you see an error like this:

```
Job for nginx.service failed because the control process exited with error code.
See "systemctl status nginx.service" and "journalctl -xeu nginx.service" for details.
```

Then see the error using the "status" command above. For instance, if you forget a semicolon, it might have an error like this:

```
Aug 27 23:04:41 archived nginx[10573]: nginx: [emerg] invalid number of arguments in "ssl_certificate_key" directive in /etc/nginx/nginx.conf:45
```

In which case, fix the line, check your work, and possibly start over by copying over the configuration again. Again, it is important to keep the first set of lines, which you can see in the very first section of this discussion of the nginx config. The ubuntu user is specific to ubuntu and needs to be `www-data`, for instance.

When it works, it says nothing. Then we can access our website! Almost!! It will say a gateway error because nothing is actually running EXCEPT the web proxy server.

We should now "enable" our nginx server on boot:

```
sudo systemctl enable nginx
```

## 10. Create and enable Occam service

Now, we can create services to run Occam on our web server.

We provide basic systemd service scripts in both `occam` and `occam-web-client` repositories. We will copy those and edit them for our needs. So, starting from the parent directory of our two repositories:

```
sudo cp occam/docs/systemd/occam.service /etc/systemd/system/
```

We want to edit that file (`sudo vim /etc/systemd/system` or similar) to point it to the right places. Assuming we are setting this up as the current user, then we want to update the `User` to be that user name.

In my case, the systemd script provided assumes that `occam` is in your path. This is not just true by default. We can make it true by adding it to our `$PATH` environment variable upon login.

```
vim ~/.profile
```

And add to the bottom:

```
export PATH=$HOME/occam/bin:$PATH
```

Now `occam` will work by itself in your terminal and also by the systemd script.

So start it:

```
sudo systemctl start occam
```

### 11. Create and enable the Occam Web Client service

Similar process. Copy over the provided script.

```
sudo cp occam/docs/systemd/occam-web-client.service /etc/systemd/system
```

Edit the service to update the `User`, if it isn't `occam`. Also change the working directory to where you installed `occam-web-client` since it runs from that directory at the moment.

Mine looks like this now:

```
[Unit]
Description=Occam Web
After=network.target

[Service]
Type=simple
# The user that will run Occam (should not be root)
User=occam
WorkingDirectory=/home/occam/occam-web-client
ExecStart=/bin/zsh -ilc /home/occam/occam-web-client/start.sh
Restart=always
RestartSec=1

[Install]
WantedBy=multi-user.target
```

Yay! Let's just run that now.

```
sudo systemctl start occam-web-client
```

You should now see something navigating to your website!

Now enable it on boot:

```
sudo systemctl enable occam-web-client
```

### 12. Set up a backend (Singularity)

You may need to remove any package installed prior or by Occam install scripts, such as singularity-ce:

```
sudo apt remove singularity-ce
```

Using the instructions found on the [Apptainer website](https://apptainer.org/docs/admin/main/installation.html), which will be more up-to-date, do this:

```
wget https://github.com/apptainer/apptainer/releases/download/v1.1.0-rc.2/apptainer_1.1.0-rc.2_amd64.deb
sudo apt-get install -y ./apptainer_1.1.0-rc.2_amd64.deb
```

Wee! Now, re-initialize Occam to see singularity:

```
cd occam
./bin/occam system initialize
```

### 13. Bootstrapping (15 minutes)

We have a basic but EMPTY Occam installation. Let's get the basics on there. Thankfully, a bootstrap script is provided.

Just navigate to the `occam` path and run it:

```
cd occam
./scripts/bootstrap.sh
```

This pulls a singularity environment, some editors and viewers, the Python template, and much of the basic Linux software environment. It pulls it from both https://occam.cs.pitt.edu and https://occam.software. Eventually it might pull from https://archived.software.

It sometimes helps clear the cache faster to restart the `occam-web-client` when you pull lots of new things like this: `sudo systemctl restart occam-web-client`.

Now you are ready to go! You should be able to complete the Getting Started guide on your own instance, now.

### 14. Other Things

**Widget Proxy Host**

It is wise to set up the public storage proxy. Previously, we set up a certificate for `proxy.archived.software`, and indeed that domain points to our web server.

When you do this, you add to your nginx configuration (`/etc/nginx/nginx.conf`) a duplicate `server` section for the server where you replace your main domain with the proxy domain.

Then, you tell the web client what that domain is by editing `~/.occam/web-config.yml` and uncommenting and updating the `widget-host` key to point to that domain. For instance, mine looks like:

```
widget-host: proxy.archived.software
```

Now, whenever an iframe is spawned for dynamic software content, such as a JavaScript-powered widget, it will be served and make requests under that domain. The web client handles such requests differently to ensure that sessions and account information is kept safe.

**Plugins**

We can insert some interesting plugins! Many can be found for the daemon [here](https://gitlab.com/occam-archive/occam-plugins) and the web client [here](https://gitlab.com/occam-archive/occam-web-plugins).

Some of them are full-fledged sites of their own that are powered by Occam underneath. Others add features or extra capability to the normal Occam platform.

To install any of these, instructions can be found in those repositories and, generally, in the `README.md` within the `plugins` directory of each main Occam repository. Specifically, just go to the [`occam/plugins`](https://gitlab.com/occam-archive/occam/-/tree/develop/plugins) and [`occam-web-client/plugins`](https://gitlab.com/occam-archive/occam-web-client/-/tree/develop/plugins) pages on GitLab.

Basically, go into the associated `plugins` directory and clone the plugins path and, for the daemon, re-initialize the system. (Unless the plugin has more specific instructions or dependencies.)

```
cd occam
cd plugins
git clone https://gitlab.com/occam-archive/occam-plugins/occam-social
cd ..
./bin/occam system initialize
```

Or for the web client:

```
cd occam-web-client
cd plugins
git clone https://gitlab.com/occam-archive/occam-plugins/occam-social
cd ..
```

And restart the web client service (`sudo systemctl restart occam-web-client`). And presto.

**Explore Domain-Specific Installations**

For web archival, we have several objects you may want to bring in. The [Tracer Script](https://occam.cs.pitt.edu/QmWiJ4bv6CXCRGUuaKp32SvvPRShZCYwE4NtQ55cTQzmBL) is a script that crawls a website and produces a WARC output based on a "trace" of clicks and such.

```
occam objects pull https://occam.cs.pitt.edu/QmWiJ4bv6CXCRGUuaKp32SvvPRShZCYwE4NtQ55cTQzmBL
```

And the [web-replay widget](https://occam.cs.pitt.edu/QmWKc6LjQ3T2fwax18XWY1kdRj3zMHwKsiZ9LbU3zPjHSC) to view the resulting WARC at the client-side:

```
occam objects pull https://occam.cs.pitt.edu/QmWKc6LjQ3T2fwax18XWY1kdRj3zMHwKsiZ9LbU3zPjHSC
```

**Scale Production**

If your system is a bit more powerful, it helps to scale the system to use more of
those resources. The daemon can run multiple instances. It will spawn a process when
a client connects to it. Clients can keep connection pools to the daemon in order to
maximize their use.

The clients themselves can run multiple instances of the web server. This is
configured by modifying the "`web-config.yml`" in the Occam root
("`$HOME/.occam/web-config.yml`") and increasing the "workers" field. The thread
pool within a worker is managed by `min-threads` and `max-threads`. Generally,
four threads and a worker for every hardware core is a pretty sensible idea. A
connection to the daemon is made for at least each worker thread.

The systemd scripts we used in this document already run the web server in
production mode. If you are doing your own thing, make sure the web server is
starting in production mode via `start.sh -e production` or by using the `RACK_ENV`
variable and setting that to "production". When in production mode, the web server
turns off some debugging features and always only sends out compiled assets. It
also makes certain session information and sandboxes more secure since the
production environment always assumes HTTPS connectivity.
