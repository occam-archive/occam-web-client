# Occam + SST VM Image

Hello! This Virtual Machine image contains a self-hosted instance of Occam
which has been pre-loaded with SST and the SST Project Template. It is presented
as a VirtualBox OVA file which can be imported into VirtualBox version 6.x. This
image contains a working and running Occam instance with SST installed and the
ability to generate boiler-plate simulations from an interactive widget. Occam,
itself, is a means of repeatably running and deploying such simulations and
contains demoable examples of such within the Virtual Machine. Specific
instructions to run, interact, build, and play are included in the
`INSTRUCTIONS` file in the home directory of the Linux account which should
automatically log in, but has the password `foobar`.

## Working with the Virtual Machine

The Virtual Machine will automatically log you in but the account is `wilkie`
with the password `foobar`. The account has sudo access and just the bare
minimum it needs to run Occam already installed. What follows is instruction
on running Occam.

The following is scripted as `run.sh` in the home directory, so all you need to
do to run an instance of Occam and bring up the website locally is to invoke:

```
~/run.sh
```

If you want manual steps, follow these below:

To run the backend, which you will need to have running in the background before
you can interface with the web client, you will need to run:

```
~/occam/bin/occam daemon run
```

And then navigate to the `occam-web-client` directory, which contains the web
portal and the front-end logic, and run its `start.sh` file:

```
cd ~/occam-web-client
./start.sh
```

This will run a webserver at [[http://localhost:9292/]] by default.

## About the Template

The Project Template gives a quick way to generate boiler-plate SST simulation
projects via generating a Python script based on a graphical visualization of
SST elements and components.

This widget interfaces with Occam's normal search functionality and archival
best practices to ensure a repeatable runtime and parallel deployment of such
SST simulations. The widget will search for and populate its interface with the
SST Elements it finds throughout the world. It does this by searching for
particular types of SST 'component' objects and reading the `sst-info.xml` file
that needs to be at the root of that object's built package. One such example is
the basic SST Elements object, which you can find by searching for "SST Elements."

You can see that, indeed, that object has a built where the sst-info.xml is a file
at that build's root when inspecting the "Build" tab for that object.

## Signing In / Creating an Account

You can sign in by using one of the accounts that already exists in this VM.

The administrator account is username `wilkie` and password `asdf`.

A user account has been made with the name `sst-user` and password `occam`.

## Building a Simulation

Go to your "Active" tab to create a project by clicking on your username in the
top bar and clicking on "Active" in the dropdown. Or navigate there from your
dashboard (the initial page when you log in) by finding "Active" in the tab
strip in purple near the top of the page.

Once in the "Active" tab, you can click on the button there that will create a
new project. Or, you can view the existing projects if you are looking at the
`sst-user` account. For the new object, you want to select the "SST Simulation"
template and give it a generic name and then press through the dialog until it
creates your boiler-plate object.

Either way, you will be presented with the SST Designer widget that will let you
generate the boiler-plate Python code. By dragging components from the sidebar
to the canvas, it will generate the corresponding Python code to instantiate
those elements and adds it as a dependency to the project itself. Since it is
now a dependency, when you share this simulation script, it will also tag it
in such a way that it will also distribute the element code along with it.

When you have two nodes on the canvas, you can link them together by clicking
and dragging to create 'wires' between them. This will cause the corresponding
`sst.Link` to be created and configured in the boiler-plate code. You may have
to click on a node to make the (+) button visible and then clicking on the
button with the plus to the left-hand side of the node will give a list of
input pins. Clicking on an input in the dropdown listing will yield the green
pin on the canvas. You can connect any two such ports.

When you have a visualization you like, you can click the floppy disk icon to
"save" the boilerplate. This updates your object and either adds or overwrites
the main Python file. You can now open that file in the file editor by
navigating to "Files" and opening the top Python file you see in the listing.
The file is "`simulation.py`"

You will have to edit this file to configure some of the nodes which cannot be
configured correctly automatically. (See Limitations below) There is no such
reasonable way to get around manual decisions, particularly when designing for
arbitrary elements and components that others might write.

## Running your Simulation

When the building is done, you can "Publish" the simulation. This saves its
state in a versioned repository and allows it to be used within a "workflow".
Go to the "History" tab of the simulation and press Publish and give it a
snappy message and away it goes.

When this is done, we can now build a workflow using our simulation. Here, we
could compose it with several other simulations or pass the results to a
plotting tool, but for now we will just run it by itself.

Create a new object (From "Active", again) and this time make a "Blank
Experiment". You will see an empty canvas, again. This time, however, we are
looking at a workflow. To the top-left there is a button that will reveal an
object selector. Slide this sidebar out using that button.

We will find our 'simulation'. First, filter by type 'simulation' and then
go to filter by name. If you see your simulation object in the dropdown
menu, just click on it. This will create the workflow node which we then
have to drag out onto the once-empty canvas.

With this in place, we can edit the configurations. Click on that node to
reveal buttons underneath the box for the simulation. The button that looks
like a set of gears is the 'configuration' button. Clicking on this will
slide out a panel to the right-hand side that will consist of a set of
options. These were populated by each of the elements that we chose to be
a part of our simulation and are somewhat based on the `sst-info.xml`
metadata.

You likely need to actually modify these values. The defaults are either
not acceptable or the types are not correctly documented. It is a severe
limitation of SST itself, and not something easily remedied without a lot
of manual effort in the widget.

When you do this (or look at the workflow in the `sst-user` account), you
can then go to the "Run" tab and click the button to run the workflow at
the bottom of that page.

It will show a version of the workflow which visualizes which parts of
the workflow are currently running. When the node is blue, it is running,
and when it is green, it finished successfully. If it is red, however, it
failed to run. To inspect the standard output of the program (what is
printed to the screen), click on the node within this run visualization
in the "Run" tab to reveal another set of buttons. The button there will
slide out a panel to the left with a list of jobs. Clicking on the one
and only job will show its output.

If it is a mis-configuration, go to the workflow and update the
configuration and try it again.

Good luck!

## Specific End-to-End Demo

Create a new "SST Simulation" from the template via the "Active" tab.

Open the side panel, if needed, when viewing the SST simulation to see
the component selector.

Select "miranda" from the component list in the side and then select
"BaseCPU" as the component. Drag it on the canvas.

Select "memHierarchy" and then "cache" and "memDirectory" and drag those
both on.

Reveal "BaseCPU"'s cache_src link, the "cache"'s high and low network
ports, and the "memDirectory"'s direct link. Attach the CPU to the
high port of the cache and the direct link to the low port of the
cache.

Press the "save" button in the editor view to create the necessary files.

Within the "Files" tab, select the `simulation.py` file and open it to
edit it. Add these lines:

```
# ...

sst.enableAllStatisticsForAllComponents()

# These lines are new:

streamN = 500000
verbose = False
BaseCPU0.addParams({
        'generator':                    'miranda.STREAMBenchGenerator',
        'generatorParams.n':            streamN,
        'generatorParams.start_a':      0,
        'generatorParams.start_b':      streamN * 32,
        'generatorParams.start_c':      2 * streamN * 32,
        'generatorParams.operandwidth': 32,
        'generatorParams.verbose':      int(verbose),
})

MemController2.addParams({
        "backend" : "memHierarchy.simpleMem",
        "backend.access_time" : "30ns",
        "do_not_back" : 1,
        })

print(BaseCPU0_config)
print(Cache1_config)
print(MemController2_config)
```

This will print the configurations and also attach the backend and
generators for creating a benchmark program. The widget cannot understand
generators. (See Limitations below)

Now save this file.

Go to the "History" tab and publish the object with some informative
message like "Created my first SST simulation!"

Now we can place it in a workflow.

Go to the "Active" tab (click your username in the top bar and select
"Active" from the dropdown.)

Create a new object. This time a "Blank Experiment" which creates a
workflow. Give it a name like "My Simulation Workflow" or the like.

Here, slide out the object selector side panel by clicking the button next
to "Main" in the tab strip above the canvas. This will give you a search
pane. Type "simulation" as the type to filter. Then start typing the name
you gave your simulation object and click on it when it appears in a drop-
down listing.

Drag it on to the canvas.

You will need to configure it. Click on the box you dragged out on to the
canvas to select it. This will reveal specific action buttons below. Click
on the one that looks like gears. This is the configure button which will
slide out the generated configuration form for the simulation.

This panel consists of tabs for each simulation element you added. We
added one for a CPU, cache, and memory controller. Therefore, we have at
least those three tabs. We need to add some configurations otherwise our
simulation will fail due to inadequate defaults. See the sections below for
values that work.

Once you configure these with the settings below, you can click "Run" and
then press the "Run" button at the bottom of that page.

### Cache Configuration

**cache_frequency**: 2GHz

**cache_size**: 32KB

**associativity**: 8

**access_latency_cycles**: 4

**L1**: Switch on (true)

### MemController Configuration

**backend.mem_size**: 16384MiB
**clock**: 200MHz

## Limitations

The widget cannot really get a full simulation going without some human
interaction. This is due to the limiting nature of the SST element metadata.
This information cannot adequately describe the options appropriately.
Sometimes options are required, and other times options have defaults that
then make other options required, and other times options are just incorrectly
documented. This means that the widget, which does some best-effort work, may
and will generally more often than not, generate Python SST code that does not
work. Most times, SST will report a failure based on configuration and that
error is not very clear what might need to change. The actor using the widget
still needs to know how to appropriately use the components and update the
generated script accordingly.
