$(function() {
  /* Returns urlsafe base64 encoding */
  function base64_urlencode(key) {
    var code = $.base64.encode(key);
    // Replace + / with - _
    code = code.replace('+', '-').replace('/', '_');
    // Remove padding
    while (code.charAt(code.length-1) == '=') {
      code = code.slice(0, code.length-1);
    }

    return code;
  }

  var url = window.document.location.protocol +
            '//' +
            window.document.location.host;

  caja.initialize({
    cajaServer: url + "/js/caja",
    debug:      true
  });

  var configurationStringToKey = function(connection, configuration, key) {
    var base = "data[" + connection + "][" + configuration + "]";
    var ret = base;

    var parts = key.split('.');

    var current = "";
    var index = 0;
    parts.forEach(function(part) {
      current = current + part;

      if (current.length > 0) {
        if (current[current.length-1] != '\\' && index < parts.length) {
          // We have a complete key
          ret = ret + "[" + base64_urlencode(current) + "]";
          current = "";
        }
      }

      index += 1;
    });

    return ret;
  };

  var getConfigurationElement = function(connection, configuration, key) {
    /* We have to do this precision to eliminate hijacking by
     * widgets trying to fool us. */
    var tag_base = "html > body > .content > .tab-panels > .tab-panel[data-connection-index=" + connection + "] > .card > .tab-panels.configurations > .tab-panel.configuration > .configuration-search-options ";

    var configuration_key = configurationStringToKey(connection, configuration, key);

    // Escape brackets
    configuration_key = configuration_key.replace(/([[\]])/g, "\\$1");

    var tag_search = tag_base + "*:not([type=hidden])[name=" + configuration_key + "]";

    return $(tag_search);
  };

  var setConfigurationOption = function(connection, configuration, key, value) {
    var element = getConfigurationElement(connection, configuration, key);

    element.val(value);
  };

  var testConfigurationOption = function(connection, configuration, key, value) {
    var element = getConfigurationElement(connection, configuration, key);

    // TODO
  };

  var getConfigurationOption = function(connection, configuration, key) {
    var element = getConfigurationElement(connection, configuration, key);

    var info = {};

    var root = element.parent();

    // Is this an enumerated value? If so, get the possible values.
    if (root.hasClass('select')) {
      root = root.parent();
      info.options = element.children('option').map(function () { return $(this).text(); }).toArray();
      info.value = element.children('option[selected=selected]').text();
    }
    else {
      info.options = [];
      if (element.attr('type') == 'checkbox') {
        info.value = element.val() == "on";
      }
      else {
        info.value = element.val();
      }
    }

    info.description = root.children('.description').text();
    info.label = root.children('label').text();
    info.type = root.children('label').attr('class');

    return info;
  };

  var loadWidget = function(widget) {
    widget.children().remove();

    var connection_index = widget.data('connection-index');
    var configurator_url = widget.data('configurator-url');
    var base_url = url + configurator_url;
    var frame_path = base_url + "/" + widget.data('configurator-file');

    var uriPolicy = {
      rewrite: function(uri) {
        if ((uri + "").substring(0, base_url.length) === base_url) {
          return uri;
        }
        return undefined;
      },
      fetch: function(uri) {
        if ((uri + "").substring(0, base_url.length) === base_url) {
          return true;
        }
        return undefined;
      }
    };

    var occam = {
      log: function(message) {
        console.log(message);
      },
      xhr: function(url, mime, callback) {
        // TODO: handle various url schemes
        url = base_url + "/" + url;
        var req = new XMLHttpRequest;
        if (arguments.length < 3) callback = mime, mime = null; else if (mime && req.overrideMimeType) req.overrideMimeType(mime);
        req.open("GET", url, true);
        if (mime) req.setRequestHeader("Accept", mime);
        req.onreadystatechange = function() {
          if (req.readyState === 4) {
            var s = req.status;
            callback(!s && req.response || s >= 200 && s < 300 || s === 304 ? req : null);
          }
        };
        req.send(null);
      },
      text: function(url, mime, callback) {
        function ready(req) {
          callback(req && req.responseText);
        }
        if (arguments.length < 3) {
          callback = mime;
          mime = null;
        }
        occam.xhr(url, mime, ready);
      },
      json: function(url, callback) {
        occam.text(url, "application/json", function(text) {
          callback(text ? JSON.parse(text) : null);
        });
      },
      html: function(url, callback) {
        occam.text(url, "text/html", function(text) {
          if (text != null) {
            var range = document.createRange();
            range.selectNode(document.body);
            text = range.createContextualFragment(text);
          }
          callback(text);
        });
      },
      xml: function(url, mime, callback) {
        function ready(req) {
          callback(req.responseXML);
        }
        if (arguments.length < 3) {
          callback = mime;
          mime = null;
        }
        occam.xhr(url, mime, ready);
      },
      appendSVG: function(url, selector, callback) {
        function ready(req) {
          var importedNode = document.importNode(req.responseXML.documentElement, true);
          // TODO: remove other events
          // TODO: (security) audit
          $(importedNode).find('*').attr('onmouseover', '');
          $(selector).append(importedNode);
          callback();
        }
        occam.xhr(url, ready);
      },

      // Retrieves the value currently in the form entry
      // for this configuration option.
      getValue: function(configuration, key) {
        var configuration_info = getConfigurationOption(connection_index, configuration, key);

        return configuration_info.value;
      },

      // Retrieves the list of possible options for an enumerated value.
      getEnumList: function(configuration, key) {
        var configuration_info = getConfigurationOption(connection_index, configuration, key);

        return configuration_info.type;
      },

      // Retrieves the metadata for the given configuration option.
      getMetadata: function(configuration, key) {
        var configuration_info = getConfigurationOption(connection_index, configuration, key);

        return configuration_info;
      },

      // Retrieves the metadata for the given configuration option.
      setValue: function(configuration, key, value) {
        return setConfigurationOption(connection_index, configuration, key, value);
      },

      testValue: function(configuration, key, value) {
        return testConfigurationOption(connection_index, configuration, key, value);
      },
    };

    caja.load(widget[0], uriPolicy, function(frame) {
      // Tame functions
      caja.markReadOnlyRecord(occam);
      caja.markFunction(occam.log);
      caja.markFunction(occam.appendSVG);
      caja.markFunction(occam.text);
      caja.markFunction(occam.json);
      caja.markFunction(occam.html);
      caja.markFunction(occam.xhr);
      caja.markFunction(occam.xml);
      caja.markFunction(occam.getValue);
      caja.markFunction(occam.getMetadata);
      caja.markFunction(occam.getEnumList);
      caja.markFunction(occam.setValue);
      caja.markFunction(occam.testValue);
      var tamedOccam = caja.tame(occam);

      var api = {
        occam: tamedOccam
      };

      frame.code(frame_path, 'text/html')
           .api(api)
           .run();
    });
  };

  $('.attach-configurator.button').on('click', function(event) {
    var button = $(this);
    var parts = button.attr('name').split('_');
    var configurator_index = parts[parts.length-1];

    var configurator_id = $('input[name=attach_configurator_id\\[' + configurator_index + '\\]]').val();
    var configurator_rev = $('input[name=attach_configurator_revision\\[' + configurator_index + '\\]]').val();
    var configurator_file = $('input[name=attach_configurator_file\\[' + configurator_index + '\\]]').val();

    var configurator_id_set = button.parent().children('input[type=hidden][name=configurator_id]');
    var configurator_rev_set = button.parent().children('input[type=hidden][name=configurator_rev]');
    var configurator_data_set = button.parent().children('input[type=hidden][name=configurator_data]');

    configurator_id_set.val(configurator_id);
    configurator_rev_set.val(configurator_rev);

    var configurator_url = "/objects/" + configurator_id + "/" + configurator_rev + "/raw";

    // Reinitialize widget box
    var widget = $('#configurator');
    widget.data('configurator-url', configurator_url);
    widget.data('configurator-file', configurator_file);

    button.remove();

    loadWidget(widget);

    event.stopPropagation();
    event.preventDefault();
  });

  $('.configurator').each(function() {
  });
});
