import 'svgxuse';

import 'nodelist-foreach-polyfill';

import { polyfill } from 'es6-object-assign';
polyfill();

import 'es7-object-polyfill';

import boundIncludesShim from 'array-includes';
boundIncludesShim.shim();

import 'string-includes-polyfill';

import 'element-remove';

import 'array-from-polyfill';

import 'ie-string-startswith-polyfill';

import Symbol from 'es6-symbol';
window.Symbol = Symbol;

import 'element-matches-polyfill';

import objectFitImages from 'object-fit-images';

window.addEventListener('load', function(_) {
    objectFitImages();
});
