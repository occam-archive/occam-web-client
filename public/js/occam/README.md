```
:::text

card.js          - Occam.Card: handles .card specific functionality.
configuration.js - Occam.Configuration: handles interacting with configurations
configurator.js  - Occam.Configurator: handles configuration widgets
data_viewer.js   - Occam.DataViewer: handles structured data/results viewer
directory.js     - Occam.Directory: handles .directory.information content tree
messager.js      - Occam.Messager
object.js        - Occam.Object: handles object interaction
paper.js         - Occam.Paper: handles our typesetting component
run_topbar.js    - Occam.RunTopbar: handles the run progress widget on the navbar
run_viewer.js    - Occam.RunViewer: handles the run progress widget
selector.js      - Occam.Selector: handles dropdown menus
tabs.js          - Occam.Tabs: handles tab strips
terminal.js      - Occam.Terminal: implements a terminal widget (with vt100)
validator.js     - Occam.Validator: wraps the form validation library
vt100.js         - Occam.VT100: implements a vt100 emulator (for Occam.Terminal)
websocket.js     - Occam.WebSocket: manages and routes websocket data
workflow.js      - Occam.Workflow: implements the workflow widget
```
