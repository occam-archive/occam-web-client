"use strict";

import EventComponent  from './event_component.js';

class TestViewer extends EventComponent {
    constructor(element) {
        super();

        if (element === undefined) {
            throw "element is not defined";
        }

        this.element = element;

        TestViewer._count++;
        this.element.setAttribute('data-test-viewer-index', TestViewer._count);
        TestViewer._loaded[this.element.getAttribute('data-test-viewer-index')] = this;

        this.initialize();
    }
    
    /**
     * Returns an instance of TestViewer for the given element.
     *
     * This will create a TestViewer, if it doesn't exist, for this element.
     *
     * @param {HTMLElement} element The main element for the calendar.
     */
    static load(element) {
        if (!element) {
          return;
        }

        var index = element.getAttribute('data-loaded-index');

        if (index) {
          return TestViewer._loaded[index];
        }

        return new TestViewer(element);
    }

    /**
     * Instantiates all TestViewer elements within the given element.
     */
    static loadAll(element) {
        var testViewers = element.querySelectorAll('.test-viewer-project');

        testViewers.forEach( (element) => {
            TestViewer.load(element);
        });
    }
}

TestViewer._count  = 0;
TestViewer._loaded = {};

export default TestViewer;
