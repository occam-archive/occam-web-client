# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2017-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Occam
  # Represents an intermediate service object.
  class Service
    # The name of the service
    attr_reader :name

    # The backend providing the service, if any
    attr_reader :backend

    # Constructs a new Service instance based on the given service metadata.
    def initialize(options = {})
      @name = options[:name]
      @type = "service"
      @id = options[:id]
      @revision = options[:revision]
      @backend = options[:backend]
    end

    # Retrieves the set of all known services
    def self.all(options = {})
      arguments  = []
      cmdOptions = {}

      if options[:query]
        cmdOptions["-s"] = options[:query]
      end

      result = Occam::Worker.perform("services", "list", arguments, cmdOptions)
      ret = JSON.parse(result[:data], :symbolize_names => true)

      if options[:json]
        ret
      else
        ret.map do |info|
          Occam::Service.new(info)
        end
      end
    end
  end
end
