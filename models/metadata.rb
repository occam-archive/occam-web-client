# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2017-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Occam
  # Represents the Occam metadata component and such plugins
  class Metadata
    # Get the list of all metadata plugins available from our backend.
    #
    # @return [Hash] Info for each metadata format.
    def self.list
      # Conserve the result (it won't likely change)
      if !defined?(@@metadata) || @@metadata.nil?
        @@metadata = nil

        # Get the metadata info
        arguments  = []
        cmdOptions = {}

        begin
          result = Occam::Worker.perform("metadata", "list", arguments, cmdOptions)
          @@metadata = JSON.parse(result[:data], :symbolize_names => true)
        rescue Occam::Daemon::Error => _
        end
      end

      @@metadata
    end

    # Retrieve the descriptive name of the metadata standard specified.
    #
    # @param key [String] The metadata format to query.
    #
    # @return [String] A short descriptive name of the standard (or nil)
    def self.nameOf(key)
      key = key.intern

      if !self.list().include?(key)
        return nil
      end

      self.list()[key][:short] || self.list()[key][:name] || key
    end

    # Retrieve a metadata record for the given object and the given format.
    #
    # Will return an array where the first value is a String giving the content
    # type of the document. The second and last value in the returned array is
    # an array of byte strings containing the document itself.
    #
    # @param object [Occam::Object] The resolved object to use.
    # @param key [String] The metadata format to query.
    #
    # @return [Array] A set of values representing the response.
    def self.view(object, key, options = {})
      arguments  = [key, object.fullID]
      cmdOptions = {}

      if options[:account]
        cmdOptions["-T"] = options[:account].token
      end

      result = Occam::Worker.perform("metadata", "view", arguments, cmdOptions)

      # Get first line as the status code
      type, rest = result[:data].split("\n", 2)

      [type, [rest]]
    end

    # Submit an HTTP request to a metadata plugin.
    #
    # Returns the typical Rack response tuple: [status, headers, data] where
    # `status` is a numeric value corresponding to the HTTP status, `headers` is
    # a dictionary of HTTP headers to add to the response, and data is an array
    # of byte strings to push out as the response body.
    #
    # @param key [String] The metadata format to query.
    # @param method [String] The HTTP method issued.
    # @param path [String] The relative path of the request.
    # @param headers [Hash] The dictionary of HTTP headers from the request.
    # @param data [String] Any post data.
    #
    # @return [Array] A set of values representing the response.
    def self.http(key, method, path, query, headers, data)
      arguments  = [key, method, path]
      cmdOptions = {}

      (query || {}).each do |k, v|
        cmdOptions["-q"] ||= []

        cmdOptions["-q"] << [k, v]
      end

      (headers || {}).each do |k, v|
        cmdOptions["-d"] ||= []

        cmdOptions["-d"] << [k, v]
      end

      result = Occam::Worker.perform("metadata", "http", arguments, cmdOptions, data)

      # Get first line as the status code
      status, headers, rest = result[:data].split("\n", 3)

      status = status.to_i

      # Get the response headers next
      headers = JSON.parse(headers)

      # And then pass along the stream as the data next
      data = [rest]

      [status, headers, data]
    end

    # Retrieves the schema for the metadata standard given.
    #
    # @param key [String] The metadata format to query.
    #
    # @return [Hash] The configuration schema or nil, if none.
    def self.schemaFor(key)
      # Conserve the result (it won't likely change)
      if !defined?(@@schema) || @@schema.nil?
        @@schema = {}
      end

      if !@@schema[key]
        # Get the metadata info
        arguments  = [key]
        cmdOptions = {}

        begin
          result = Occam::Worker.perform("metadata", "schema", arguments, cmdOptions)
          @@schema[key] = JSON.parse(result[:data], :symbolize_names => true)
        rescue Occam::Daemon::Error => _
        end
      end

      @@schema[key]
    end
  end
end
