# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2017-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Occam
  require_relative 'object'

  # This wraps functionality for a Workflow, which is a particular type of
  # Object.
  class Configuration < Occam::Object
    # Updates the given configuration with the given data
    #
    # If data is an array, it is set via keys. The contents of the array are
    # a set of key, value tuples.
    #
    # If you wish to replace a file
    # completely with the contents of a hash, convert it directly to a json
    # string first and supply the string as the data parameter.
    def configure(data, options={})
      cmdOptions = {}
      if @account
        cmdOptions["-T"] = @account.token
      end
      cmdOptions["-j"] = true

      cmdOptions["-t"] = options[:type] || "text"

      stdinData = nil

      if data.is_a? Array
        data.each do |k, v|
          cmdOptions["-i"] ||= []
          if v.nil?
            cmdOptions["-i"] << [k]
          else
            cmdOptions["-i"] << [k, v]
          end
        end
      else
        cmdOptions["-"] = true
        stdinData = data
      end

      if options[:message]
        cmdOptions["-m"] = options[:message]
      end

      result = Occam::Worker.perform("configurations", "set", [self.fullID], cmdOptions, stdinData)
      data = JSON.parse(result[:data], :symbolize_names => true)

      root = nil
      ret = nil
      if data[:updated]
        root = data[:updated][0]
        index = data[:updated][1..-1].map do |item|
          item[:position]
        end

        ret = self.class.new(:id => root[:id], :revision => root[:revision], :index => index, :account => @account, :link => @link, :roots => data[:updated][0..-2], :root => root, :info => data[:updated][-1])
      end

      ret
    rescue Exception => e
      puts e
      nil
    end

    # Returns the value from the hash given a nesting.
    # hash = {
    #   "a" => 3,
    #   "b" => "foo"
    #   "c" => {
    #     "1" => 42,
    #     "w" => "wilkie"
    #   }
    # }
    #
    # resolveKey(hash, "c.1")
    #   => 42
    def self.resolveKey(hash, nesting)
      if nesting.nil?
        return nil
      end

      keys = nesting.split('.')

      current = hash

      keys.each do |k|
        k, *indices = k.split(/(?<!\\)\[/, 2)
        if indices.length == 0
          indices = ""
        else
          indices = indices[0]
        end

        if !current.nil?
          current = current[k]
        end

        parts = indices.split(/(?<!\\)\]\[/)
        parts.each do |array_index|
          current = current[array_index.to_i]
        end
      end

      return current
    end

    # Returns the encoded id of the given element.
    #
    # This gets used in configuration forms since keys can be complex strings.
    def self.safeNameFor(prefix, nesting)
      id = prefix

      if !nesting.nil?
        keys = nesting.split('.')

        keys.each do |k|
          inner = Base64.urlsafe_encode64(k)
          id = "#{id}[#{inner}]"
        end
      end

      id = Base64.urlsafe_encode64(id)

      id
    end

    # Yields the expected schema that can represent the given data.
    def self.schemaFor(data)
      schema = {}

      # For every data item, determine the item type
      if data.is_a?(Array)
        schema[:type] = "array"

        # We attempt to get an understanding of the possible schema
        subschema = {}
        complex = false
        data.each do |item|
          if item.is_a?(Hash)
            complex = true
          end
          subschema.update(Occam::Configuration.schemaFor(item))
        end

        if !complex
          subschema = subschema[:type]
        end

        schema[:element] = subschema
      elsif data.is_a?(Hash)
        if data.has_key?(:r) && data.has_key?(:g) && data.has_key?(:b) && data.has_key(:hex)
          schema[:type] = "color"
        else
          data.each do |k, v|
            schema[k] = Occam::Configuration.schemaFor(v)
          end
        end
      elsif data == true || data == false
        schema[:type] = "boolean"
      elsif data.is_a?(Integer)
        schema[:type] = "int"
      elsif data.is_a?(Numeric)
        schema[:type] = "number"
      else
        # It could be a date, time, etc... we won't try that hard
        schema[:type] = "string"
      end

      return schema
    end

    # Yields a schema with a little bit of everything.
    def self.testSchema
      {
        # A single text input
        :foo => {
          :type => "string",
          :default => "hello",
          :label => "A String Field",
          :description => "**hello**"
        },

        # An integer that is not validated
        :unrestrictiveInteger => {
          :type => "int",
          :default => 0,
          :label => "Integer",
          :description => "This integer is not restricted by any validations other than being a number."
        },

        # An integer with units
        :unitInteger => {
          :type => "int",
          :units => "things",
          :default => 0,
          :label => "Unit Integer",
          :description => "This integer is not restricted by any validations other than being a number and it has a unit."
        },

        # An integer that is validated for range
        :restrictiveInteger => {
          :type => "int",
          :default => 0,
          :label => "Integer (-5 to 5)",
          :description => "This integer is restricted by a range between -5 and 5.",
          :validations => [
            {
              :min => -5,
            },
            {
              :max => 5,
            }
          ]
        },

        # An integer that is validated for a specific result
        :powerfulInteger => {
          :type => "int",
          :default => 1,
          :label => "Integer (Power of 2)",
          :description => "This integer is restricted by being a power of two.",
          :validations => [
            {
              :test => "log2(x) == floor(log2(x))",
              :message => "Must be a power of two."
            }
          ]
        },

        # A color picker
        :colorNoLabel => {
          :type => "color",
          :default => "888"
        },

        :date => {
          :type => "date",
          :label => "Date"
        },

        :time => {
          :type => "time",
          :label => "Time"
        },

        :datetime => {
          :type => "datetime",
          :label => "Date and Time"
        },

        # A drop down item
        :dropdown => {
          :type => ["Item 1", "Item 2", "Item 3"],
          :label => "A Dropdown List"
        },

        # A group
        :bar => {
          :baz => {
            :type => "string",
            :label => "Ok",
            :default => "no",
          },
          :chaz => {
            :type => "color"
          },
          :blerb => {
            :type => "boolean"
          }
        },

        # A tuple of an int and enum
        # TODO: add validations to each and descriptions, perhaps
        :tuple => {
          :type => "tuple",
          :elements => [
            {
              :type => "int",
              :default => 2048
            },
            {
              :type => ["MB", "GB"],
              :default => "MB"
            }
          ],
          :label => "A Tuple",
          :description => "This is a specified set of different types connected as a single value. It will be represented as an array in the resulting JSON configuration and is considered a variant tuple when parsed out."
        },

        # An array of simple items of one type
        :array => {
          :type => "array",
          :element => "string",
          :label => "An Array of Strings",
          :description => "This is an *array* of items where each item is a simple string type."
        },

        # A more complicated array!
        :complexArray => {
          :type => "array",
          :description => "This holds several items but *just* at a single depth.",
          :element => {
            :item1 => {
              :label => "Item 1",
              :default => 0,
              :description => "This item has a validation for `[-5, 5]`",
              :validations => [
                {
                  :min => -5,
                },
                {
                  :max => 5,
                }
              ],
              :type => "int"
            },
            :item2 => {
              :label => "Item 2",
              :type => "boolean"
            }
          }
        },

        # A special case that can only happen internally
        :targets => {
          :type => :target
        },

        # Boolean and enum types can enable/disable show/hide other items

        # This entry will enable a setting
        :enablesSomething => {
          :label => "Enable the next input",
          :type => "boolean"
        },

        # This one is disabled because of a configuration option
        :enabledInput => {
          :label => "Enabled Conditionally",
          :enabledBy => {
            :enablesSomething => true
          },
          :type => "int",
          :default => 0
        },

        :revealsSomething => {
          :label => "Shows a different input",
          :type => ["int", "boolean"],
          :default => "int"
        },

        :revealedInt => {
          :label => "Revealed 'int' Option",
          :type => "int",
          :revealedBy => {
            :revealsSomething => "int"
          }
        },

        :revealedBoolean => {
          :label => "Revealed 'boolean' Option",
          :type => "boolean",
          :revealedBy => {
            :revealsSomething => "boolean"
          }
        },

        :enabledGroup => {
          :enabledBy => {
            :revealsSomething => "int"
          },

          :label => "Group for the int value",

          :item => {
            :type => 'int',
            :default => 0,
            :label => "Also enabled by the above",
            :enabledBy => {
              :enablesSomething => true
            }
          }
        },

        :revealedGroup => {
          :revealedBy => {
            :revealsSomething => "boolean"
          },

          :label => "Group for the boolean value",

          :item => {
            :type => 'int',
            :default => 0,
            :label => "Also enabled by the above",
            :enabledBy => {
              :enablesSomething => true
            }
          }
        }
      }
    end
  end
end
