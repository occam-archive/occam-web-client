class Occam
  # This represents a test suite.
  class TestRun
    attr_reader :file

    # This returns an instantiated TestRun for the given project.
    #
    # @param [String] project The project identifier. Can be "daemon", "client",
    #                         or "javascript"
    # @return [TestRun, nil] The TestRun or nil if the test suite is not
    #                        available.
    def self.for(project)
      case project
      when "daemon"
        self.forDaemon
      when "client"
        self.forClient
      when "javascript"
        self.forJavaScript
      else
        nil
      end
    end

    # Returns an instantiated TestRun for the JavaScript test suite.
    #
    # @return [TestRun, nil] The TestRun or nil if the test suite is not
    #                        available.
    def self.forJavaScript()
      if defined?(@@occamJSSpecPath) and defined?(@@occamJSSpecPathTime) and @@occamJSSpecPath
        if @@occamJSSpecPathTime < File.mtime(@@occamJSSpecPath)
          @@occamJSSpecPath = nil
          @@js_test_run = nil
        end
      end

      if !defined?(@@occamJSSpecPath) || @@occamJSSpecPath.nil?
        @@occamJSSpecPath = nil
        @@occamJSSpecPathTime = Time.now

        path = File.realpath(File.join(File.dirname(__FILE__), "..", "spec", "js", "tests.json"))

        if File.exist?(path)
          @@occamJSSpecPath = File.realpath(path)
        end
      end

      specPath = @@occamJSSpecPath

      if !defined?(@@js_test_run) || @@js_test_run.nil? || @@js_test_run.file.nil?
        @@js_test_run = nil

        # Only parse the test suite once per server run
        if specPath
          @@js_test_run = Occam::TestRun.new(:file => specPath)
        end
      end

      @@js_test_run
    end

    # Returns an instantiated TestRun for the Web Client test suite.
    #
    # @return [TestRun, nil] The TestRun or nil if the test suite is not
    #                        available.
    def self.forClient()
      if defined?(@@occamClientSpecPath) and defined?(@@occamClientSpecPathTime) and @@occamClientSpecPath
        if @@occamClientSpecPathTime < File.mtime(@@occamClientSpecPath)
          @@occamClientSpecPath = nil
          @@client_test_run = nil
        end
      end

      if !defined?(@@occamClientSpecPath) || @@occamClientSpecPath.nil?
        @@occamClientSpecPath = nil
        @@occamClientSpecPathTime = Time.now

        path = File.realpath(File.join(File.dirname(__FILE__), "..", "spec", "tests.json"))

        if File.exist?(path)
          @@occamClientSpecPath = File.realpath(path)
        end
      end

      specPath = @@occamClientSpecPath

      if !defined?(@@client_test_run) || @@client_test_run.nil? || @@client_test_run.file.nil?
        @@client_test_run = nil

        # Only parse the test suite once per server run
        if specPath
          @@client_test_run = Occam::TestRun.new(:file => specPath)
        end
      end

      @@client_test_run
    end

    # Returns an instantiated TestRun for the Occam Daemon test suite.
    #
    # @return [TestRun, nil] The TestRun or nil if the test suite is not
    #                        available.
    def self.forDaemon()
      if defined?(@@occamSpecPath) and defined?(@@occamSpecPathTime) and @@occamSpecPath
        if @@occamSpecPathTime < File.mtime(@@occamSpecPath)
          @@occamSpecPath = nil
          @@server_test_run = nil
        end
      end

      if !defined?(@@occamSpecPath) || @@occamSpecPath.nil?
        @@occamSpecPath = nil
        @@occamSpecPathTime = Time.now

        path = Occam::System.daemonPath("tests.json")

        if File.exist?(path)
          @@occamSpecPath = File.realpath(path)
        end
      end

      specPath = @@occamSpecPath

      if !defined?(@@server_test_run) || @@server_test_run.nil? || @@server_test_run.file.nil?
        @@server_test_run = Occam::TestRun.new()

        # Only parse the test suite once per server run
        if specPath
          @@server_test_run = Occam::TestRun.new(:file => specPath)
        end
      end

      @@server_test_run
    end

    # Create a TestRun based on the test results within the given file path.
    # @param [Hash] options The options to use to instantiate the TestRun.
    # @option options [String] :file The path to the test results JSON.
    def initialize(options = {})
      @file = options[:file]
    end

    # Returns the parsed JSON data as a Hash.
    # @return [Hash, nil] The JSON data or nil if the data cannot be parsed.
    def parse
      if !defined?(@data)
        if @file
          begin
            f = File.open(@file, "r")
            @data = JSON.parse(f.read(), :symbolize_names => true)
            f.close
          rescue
            @data = nil
          end
        end
      end

      @data
    end

    # Returns a tagged list of TestGroup objects represented by this TestRun.
    # @return [Hash] A dictionary of TestGroup instances based on the group keys.
    def groups
      if !defined?(@groups) and self.parse
        @groups = {}
        self.parse[:groups].each do |key, group|
          @groups[key] = Occam::TestGroup.new(group.update(:key => key))
        end
      end

      @groups || {}
    end

    # Retrieves the specific TestGroup within this set with the given key.
    # @param [String] name The key of the requested group.
    # @return [TestGroup, nil] The TestGroup requested or nil if the group
    #                          cannot be found.
    def group(name)
      self.groups[name]
    end

    # Retrieves the overall test status for this run.
    # @return [String, nil] The status text or nil if none is specified.
    def status
      (self.parse || {})[:status]
    end

    # Retrieves the metadata for this run.
    # @return [Hash] The metadata table. Default: {}
    def metadata
      (self.parse || {})[:metadata] || {}
    end

    # Retrieves the statistics data for this run.
    #
    # The data should consist of one or more of the following statistics:
    # :total is the number of tests overall.
    # :assertions is the number of assertions.
    # :failures is the number of failed tests.
    # :errors is the number of errored tests.
    # :skips is the number of skipped tests.
    # :passes is the number of passing tests.
    #
    # @return [Hash] The statistics data table. Default: {}
    def statistics
      (self.parse || {})[:statistics] || {}
    end

    # Retrieves the timing data for the run.
    #
    # The data should consist of zero or more of the following:
    # :total_seconds is the total time the tests took to run in seconds.
    # :runs_per_second is the frequency of tests per second.
    # :assertions_per_second is the frequency of assertions per second.
    #
    # @return [Hash] The timings data table. Default: {}
    def timings
      (self.parse || {})[:timings] || {}
    end
  end
end
