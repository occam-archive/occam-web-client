require_relative "helper"

feature "Creating Objects" do
  scenario "Failing to create an object with a blank name" do
    username = "wilkie-#{uuid()}"
    password = "foobar-#{uuid()}"

    account = Occam::Account.create(username, password)
    person  = account.person

    visit '/login'

    fill_in 'username', :with => username
    fill_in 'password', :with => password

    click_button "login"

    # We should be on the login page
    assert current_path == "/people/#{person.identity.uri}",
      "We failed to login"

    # Go to the collection
    find("ul.tabs > li.tab > a[href=\"/people/#{person.identity.uri}/collection\"]").click

    # Go to the object creation form
    find("a[href=\"/new\"]").click

    # Create an 'object'
    fill_in "new-object-type", :with => "object"

    # With a blank name
    fill_in "new-object-name", :with => ""
    click_button :name => "add"

    # We should NOT be on the object page
    # We should still see the form and an error
    assert page.has_css?(".errors"),
      "Cannot see an error list after attempting to create the object."
    assert page.has_css?("input#new-object-type"),
      "Cannot find the new object form."
  end
end
