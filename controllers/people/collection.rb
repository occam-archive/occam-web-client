# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2017-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require_relative "../people"
require_relative "../search"

# @api api
# @!group People API
class Occam
  module Controllers
    # These routes handle the managing of the active objects for the logged in Person.
    class PersonCollectionController < Occam::Controllers::PersonController
      get '/people/:identity/collection/?' do
        person = Occam::Person.fromIdentity(params[:identity],
                                            :account  => current_account)

        if not person
          status 404
          return
        end

        searchResult = performSearch(:excludeTypes => ["task", "person"])

        format = request.preferred_type(['text/html', 'application/json', 'application/atom+xml', 'application/xml'])

        case format
        when 'application/json'
          {
            "types" => types.map do |type|
              type.update(:icon => Occam::Object.iconURLFor(type[:type]),
                          :smallIcon => Occam::Object.iconURLFor(type[:type], :small => true))
            end,
            "objects" => objects.map do |object|
              object.info.update(:revision  => object.revision,
                                 :icon      => object.iconURL,
                                 :smallIcon => object.iconURL(:small => true))
            end
          }.to_json
        when 'text/html'
          renderPersonView("collection", searchResult)
        end
      end
    end
  end

  use Controllers::PersonCollectionController
end
