# Occam Web Plugins

This directory will contain any extensions to the web client. These plugins are
generally separately maintained repositories that are installed simply by
cloning them here in this directory.

For instance, the `occam-web-memento` plugin found
[here](https://gitlab.com/occam-archive/occam-web-plugins/occam-web-memento)
would be installed by cloning the repository to the path
`<occam web root>/plugins/occam-web-memento`.

In this case, the Occam Web Client application would pull in any controllers
found in that plugin path's `controllers` directory.

## Path Conventions

A plugin will provide certain extensions in particular directories that match
those found in the normal application.

* `controllers/*.rb`: A set of controllers that inherit from `Occam::Controller`
or, if extending from an existing set of routes, the appropriate controller
class. These will automatically be loaded (and reloaded). One needs to load the
controller class via `use` in the Occam class, however.
* `views/*.slim`: A set of slim templates that define the layouts of any new
pages within the plugin.
* `stylesheets/*.scss`: The source stylesheets written with SCSS to define any
new styling of your pages or components.

The default configuration, however, will use content within the normal web
application environment. This is generally not what you want. To force your
plugin to use relative paths, look at the example below.

## Reloader Considerations

When the application is running in development mode, the source for a plugin's
controllers, helpers, etc, will be reloaded when the server notices the source
file has been changed. Special care, then, must be taken such that parsing and
executing that code twice in one session does not have unintended side-effects.
For instance, if one is overriding a method, one should check to see if the
method has already been overridden before doing so. Otherwise, it may then, if
using `alias_method`, become self-recursing. When it is necessary to redo such
an operation, then one might need to restart the normal server, however.

## Example

If you want a very simple plugin to render your own page, then just create a
simple controller to attach something to a route.

Create the directory structure:

```
mkdir ./plugins/occam-web-my-plugin/controllers -p
```

Then, open a controller file `./plugins/occam-web-my-plugin/controllers/routes.py`
with your editor and add the following code:

```ruby
class Occam
  module Plugins
    module MyPlugin
      module Controllers
        class MyPluginController < Occam::Controllers::Controller
          get '/my-route' do
            "hello"
          end
        end
      end
    end
  end

  use Plugins::MyPlugin::Controllers::MyPluginController
end
```

When you restart the web server, you will be able to navigate to the `/my-route`
URL off of your locally running Occam instance. It will simply say 'hello'.

### Templating

To add template support and keep those files within the directory structure of
the plugin, you can update the configuration for the template within the
Controller class:

```ruby
class Occam
  module Plugins
    module MyPlugin
      module Controllers
        class MyPluginController < Occam::Controllers::Controller
          # Get our views relative to the plugin source
          rootPath = File.realpath(File.join(File.dirname(__FILE__), ".."))
          set :slim, :views => File.join(rootPath, "views")

          # Use the layout.slim as the main source for content/structure of
          # the page. Everything rendered will be within the `yield` inside
          # this layout.slim file.
          set :slim, :layout => :layout

          get '/my-route' do
            render :slim, :index
          end
        end
      end
    end
  end

  use Plugins::MyPlugin::Controllers::MyPluginController
end
```

Now you need to add `layout.slim` and, in this case, `index.slim` to your plugin
before it can render them.

Create the views directory:

```
mkdir ./plugins/occam-web-my-plugin/views -p
```

Open a layout file `./plugins/occam-web-my-plugin/views/layout.slim` and write:

```slim
doctype html
html lang="en-US"
  head
  body
    p
      | Main Content
    == yield
```

Then we can write `./plugins/occam-web-my-plugin/views/index.slim` for the page
content:

```slim
p
  | Index Page
```

### Partials and Helpers

By virtue of subclassing an Occam controller, you get access to all of the
Occam models and classes. You can refer to the main codebase for examples of how
to interact with the repositories.

One thing you get are the helpers. The main one is the `partial` helper method.
This lets you reuse parts of page renderings across your pages by creating a
template file for just that part.

We can create a file in `views` that has the name `_list.slim` (note the
underscore to start the name) which we can then add to any page by invoking the
`partial` helper:

```slim
/ index.slim

h1
  | My listing

/ Embed _list.slim
== partial(:list, :locals => {:list => [1,2,3]})

.another-list
  / Embed _list.slim again
  == partial(:list, :locals => {:list => [9,8,7]})
```

Which will embed, essentially, the slim found in the `_list.slim` file, where
the variables are set to ones passed via `:locals` and any class variables (ones
in the form `@myvar`) are just normally accessible.

```slim
/ _list.slim

ul
  - list.each do |item|
    li
      p
        = item
```

### Stylesheets

Now, we can also provide custom stylesheets for our custom pages. We add the
content after the comment within our controller class. Perhaps, after the
content defining the slim templates.

```ruby
class Occam
  module Plugins
    module MyPlugin
      module Controllers
        class MyPluginController < Occam::Controllers::Controller
          rootPath = File.realpath(File.join(File.dirname(__FILE__), ".."))

          # ...

          # SASS support for our plugin
          require 'sass/plugin/rack'
          Sass::Plugin.options[:template_location] = File.join(rootPath, "stylesheets")
          Sass::Plugin.options[:css_location]      = "./public/stylesheets/plugins/my-plugin"
          use Sass::Plugin::Rack
        end
      end
    end
  end

  use Plugins::MyPlugin::Controllers::MyPluginController
end
```

This looks for `scss` files within our plugin path's `stylesheets` subdirectory.
It will then create and publish `css` files in the occam-web-client's `public`
path in the path we specify. We generally want it to be within
`public/stylesheets/plugins/` so it does not conflict with the normal
stylesheets of the app itself or other plugins.

We then have to change our layout to pull in the `css` file. Modify
`layout.slim` to add the `link` element:

```slim
doctype html
html lang="en-US"
  head
    link rel='stylesheet' type='text/css' href='/stylesheets/plugins/my-plugin/styles.css'
  body
    p
      | Main Content
    == yield
```

Of course, we now need to add the actual stylesheets. So, create the directory
for the source files:

```
mkdir ./plugins/occam-web-my-plugin/stylesheets -p
```

And then add a stylesheet `./plugins/occam-web-my-plugin/stylesheets/styles.scss`:

```scss
  p {
    color: red;

    &:not(:first-child) {
      color: blue;
    }
  }
```

Restart the server and see the changes at `/my-route`.

### Localization

Since plugins are based on Occam Web Client, they may use the localization
strings within that project just fine. If you would like to add localization
specific to your plugin, you may add the `yml` files to a `locales` directory
relative to your plugin source. The Occam Web Client application will pull in
any such files from that specific place.

It may be possible to conflict localization keys. In order to prevent this, it
should be taken into consideration and your localization should be placed
under your own key. For instance, using our example plugin, we might create a
localization file within `./plugins/occam-web-my-plugin/locales' as such:

```
mkdir ./plugins/occam-web-my-plugin/locales -p
```

And within the file `./plugins/occam-web-my-plugin/locales/en.yml`, we would add
the English localization:

```yaml
en:
  my-plugin:
    hello: "Hello!"
```

And then I can use it within a view template, such as our index.slim:

```slim
/ index.slim

h1
  = I18n.t('my-plugin.hello')

/ Embed _list.slim
== partial(:list, :locals => {:list => [1,2,3]})

.another-list
  / Embed _list.slim again
  == partial(:list, :locals => {:list => [9,8,7]})
```

We will need to restart the web server in order for it to re-parse the
localization files since it caches those on start-up. Do this and
navigate to that index page and you should see the localized string.

If you see a 'translation missing' message instead, check that the path and the
key match the `locales` directory mentioned and what you have written for your
key and try again. Note that you will need to restart between each attempt.

### Assets

Plugin static assets should be separate from normal Occam Web Client static
assets. These include images, compiled JavaScript, and other metadata. Just
like the normal server, these are usually placed in a `public` directory. We can
do that, too.

So, let's add an asset path:

```
mkdir ./plugins/occam-web-my-plugin/public -p
```

Place an image there. We can use one from the normal client, just to test
things:

```
cp ./public/images/static/404.png ./plugins/occam-web-my-plugin/public/.
```

And then a route to handle the assets specific to our plugin:

```ruby
class Occam
  module Plugins
    module MyPlugin
      module Controllers
        class MyPluginController < Occam::Controllers::Controller
          # Get our views relative to the plugin source
          rootPath = File.realpath(File.join(File.dirname(__FILE__), ".."))

          # Handle static assets
          get '/my-plugin/assets/*' do |path|
            send_file File.join(rootPath, "public", path)
          end
        end
      end
    end
  end

  use Plugins::MyPlugin::Controllers::MyPluginController
end
```

And then we can use it, and any other static content, by using URLs that point
to that path. In this case, the URL "`/my-plugin/assets/404.png`" will feed
that image.

So, add that image to a page:

```slim
/ index.slim

h1
  = I18n.t('my-plugin.hello')

p
  img src="/my-plugin/assets/404.png"

/ Embed _list.slim
== partial(:list, :locals => {:list => [1,2,3]})

.another-list
  / Embed _list.slim again
  == partial(:list, :locals => {:list => [9,8,7]})
```

Restart the server and see the result.
