# Occam: Locales

In order to add support for many languages, the text used throughout the site
has been separated from the actual page layouts in "views" and placed here.

Each section of the site, referenced with a directory layout that parallels
that used in the "views" directory, has an associated layout file when there
is text used within it. Each "view" has a directory with a set of YAML files
for each language that has been localized.

Each language is specified by a YAML file named for its two or three letter
language code. For instance, general English is `en.yml`, but further
specialization could be done with a compound code such as `en-GB.yml`.

The actual content of the YAML file will consist of sections that correspond to
the page in question rooted in the language tag. Refer to the existing locale
files for structure.

## Precedence

When a country code is available, that will have the highest priority. Then,
the system will fall back to the language code. For example, `:es-mx` is applied
before `:es` which would be applied before `:en`.

English `:en` is the default locale. When a locale is requested, but no locale
has been provided here, then English will be used instead. Many default text
will be within this locale such as units and other general text.

This default setting is applied in `lib/application.rb` when configuring the
I18n library.
